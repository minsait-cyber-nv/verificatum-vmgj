
# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum Multiplicative Groups library for
# Java (VMGJ). VMGJ is NOT free software. It is distributed under
# Verificatum License 1.0 and Verificatum License Appendix 1.0 for
# VMGJ.
#
# You should have agreed to this license and appendix when
# downloading VMGJ and received a copy of the license and appendix
# along with VMGJ. If not, then the license and appendix are
# available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMGJ
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VMGJ in any way and you must delete
# VMGJ immediately.

# Two files define the build-process of this package:
#
# configure.ac and Makefile.am

.PHONY: clean dist

all: .build.bstamp
.build.bstamp:
	cp VERIFICATUM_LICENSE_1.0 VERIFICATUM_LICENSE_1.0_APPENDIX_VMGJ .macros.m4 .version.m4 native/
	$(MAKE) -C native -f Makefile.build
	mkdir -p m4
	cp src/m4/* m4/
	aclocal -I m4
	autoconf
	automake --add-missing --force-missing --copy
	@touch .build.bstamp

dist: .build.bstamp
	./configure --disable-check_gmpmee
	$(MAKE) dist

api: .build.bstamp
	./configure --disable-check_gmpmee
	$(MAKE) api

clean:
	-$(MAKE) clean
	$(MAKE) -C native -f Makefile.build clean
	@find . -name "*~" -delete
	@rm -rf aclocal.m4 autom4te.cache config.guess config.h config.h.in config.log config.status config.sub configure depcomp install-sh libtool ltmain.sh m4 Makefile.in Makefile missing stamp-h1 INSTALL confdefs.h conftest.tar configure.lineno vmgj-*.tar.gz *.bstamp .*.bstamp *.stamp

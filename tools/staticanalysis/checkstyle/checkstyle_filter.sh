#!/bin/sh

# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum Multiplicative Groups library for
# Java (VMGJ). VMGJ is NOT free software. It is distributed under
# Verificatum License 1.0 and Verificatum License Appendix 1.0 for
# VMGJ.
#
# You should have agreed to this license and appendix when
# downloading VMGJ and received a copy of the license and appendix
# along with VMGJ. If not, then the license and appendix are
# available at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VMGJ
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VMGJ in any way and you must delete
# VMGJ immediately.

RAW_REPORT_FILE=$1
REPORT_FILE=$2

cat $RAW_REPORT_FILE | grep -E -v "Starting audit|Audit done" | sed "s/.*verificatum\/\(verificatum.*\)/\1/p" > $REPORT_FILE

return 0
